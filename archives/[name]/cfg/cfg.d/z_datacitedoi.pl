#Enable the plugin
$c->{plugins}{"Export::DataCiteXML"}{params}{disable} = 0;
$c->{plugins}{"Event::DataCiteEvent"}{params}{disable} = 0;

#which field do use for the doi
$c->{datacitedoi}{eprintdoifield} = "id_number";
#$c->{datacitedoi}{eprintdoifield} = "doi";

#When should you register/update doi info.
$c->{datacitedoi}{eprintstatus} = {inbox=>0,buffer=>1,archive=>1,deletion=>0};

#set these (you will get the from data site)
# doi = {prefix}/{repoid}/{eprintid}
$c->{datacitedoi}{prefix} = "10.4124";
$c->{datacitedoi}{repoid} = $c->{host};
$c->{datacitedoi}{apiurl} = "https://test.datacite.org/mds/";
$c->{datacitedoi}{user} = "username";
$c->{datacitedoi}{pass} = "password";

# datacite requires a Publisher 
# The name of the entity that holds, archives, publishes, 
# prints, distributes, releases, issues, or produces the 
# resource. This property will be used to formulate the 
# citation, so consider the prominence of the role.
# eg World Data Center for Climate (WDCC); 	
$c->{datacitedoi}{publisher} = "Name of institution";

# need to map eprint type (article, dataset etc.) to ResourceType
# Controlled list http://schema.datacite.org/meta/kernel-2.2/doc/DataCite-MetadataKernel_v2.2.pdf
# where v is the ResourceType and a is the resourceTypeGeneral
#$c->{datacitedoi}{typemap}{article} = {v=>'Article',a=>'Text'};
#$c->{datacitedoi}{typemap}{book_section} = {v=>'BookSection',a=>'Text'};
#$c->{datacitedoi}{typemap}{monograph} = {v=>'Monograph',a=>'Text'};
#$c->{datacitedoi}{typemap}{thesis} = {v=>'Thesis',a=>'Text'};
#$c->{datacitedoi}{typemap}{book} = {v=>'Book',a=>'Text'};
#$c->{datacitedoi}{typemap}{patent} = {v=>'Patent',a=>'Text'};
#$c->{datacitedoi}{typemap}{artefact} = {v=>'Artefact',a=>'PhysicalObject'};
#$c->{datacitedoi}{typemap}{performance} = {v=>'Performance',a=>'Event'};
#$c->{datacitedoi}{typemap}{composition} = {v=>'Composition',a=>'Sound'};
#$c->{datacitedoi}{typemap}{image} = {v=>'Image',a=>'Image'};
#$c->{datacitedoi}{typemap}{experiment} = {v=>'Experiment',a=>'Text'};
#$c->{datacitedoi}{typemap}{teaching_resource} = {v=>'TeachingResourse',a=>'InteractiveResource'};
#$c->{datacitedoi}{typemap}{other} = {v=>'Misc',a=>'Collection'};
#$c->{datacitedoi}{typemap}{dataset} = {v=>'Dataset',a=>'Dataset'};
#$c->{datacitedoi}{typemap}{audio} = {v=>'Audio',a=>'Sound'};
#$c->{datacitedoi}{typemap}{video} = {v=>'Video',a=>'Film'};
$c->{datacitedoi}{typemap}{data_collection} = {v=>'Dataset',a=>'Dataset'};

# Additional fields for enriching DataCite metadata for subjects.  You may use a different scheme.
$c->{datacitedoi}{subjecttree} = "RCUK Research Classifications";
$c->{datacitedoi}{subjecttreeuri} = "http://www.rcuk.ac.uk/research/efficiency/researchadmin/harmonisation/";

# Additional map for licence URIs
$c->{datacitedoi}{licenceuri}{cc_by_nd} = {u=>"http://creativecommons.org/licenses/by-nd/3.0/",d=>"Creative Commons Attribution-NoDerivatives 3.0 License"};
$c->{datacitedoi}{licenceuri}{cc_by} = {u=>"http://creativecommons.org/licenses/by/3.0/",d=>"Creative Commons Attribution 3.0 License"};
$c->{datacitedoi}{licenceuri}{cc_by_nc} = {u=>"http://creativecommons.org/licenses/by-nc/3.0/",d=>"Creative Commons Attribution-NonCommercial 3.0 License"};
$c->{datacitedoi}{licenceuri}{cc_by_nc_nd} = {u=>"http://creativecommons.org/licenses/by-nc-nd/3.0/",d=>"Creative Commons Attribution-NonCommercial-NoDerivatives 3.0 License"};
$c->{datacitedoi}{licenceuri}{cc_by_nc_sa} = {u=>"http://creativecommons.org/licenses/by-nc-sa/3.0/",d=>"Creative Commons Attribution-NonCommercial-ShareAlike 3.0 License"};
$c->{datacitedoi}{licenceuri}{cc_by_sa} = {u=>"http://creativecommons.org/licenses/by-sa/3.0/",d=>"Creative Commons Attribution-ShareAlike 3.0 License"};
$c->{datacitedoi}{licenceuri}{cc_public_domain} = {u=>"http://creativecommons.org/publicdomain/zero/1.0/",d=>"Creative Commons Public Domain Dedication"};
$c->{datacitedoi}{licenceuri}{odc_by} = {u=>"http://opendatacommons.org/licenses/by/1.0/",d=>"Open Data Commons Attribution License (ODC-By) v1.0"};
$c->{datacitedoi}{licenceuri}{odc_odbl} = {u=>"http://opendatacommons.org/licenses/odbl/1-0/",d=>"Open Database License (ODbL) v1.0"};
$c->{datacitedoi}{licenceuri}{odc_dbcl} = {u=>"http://opendatacommons.org/licenses/dbcl/1-0/",d=>"Database Contents License (DbCL) v1.0"};
$c->{datacitedoi}{licenceuri}{cc_gnu_gpl} = {u=>"http://www.gnu.org/copyleft/gpl.html",d=>"GNU General Public License"};
$c->{datacitedoi}{licenceuri}{cc_gnu_lgpl} = {u=>"http://www.gnu.org/copyleft/lgpl.html",d=>"GNU Lesser General Public License"};



