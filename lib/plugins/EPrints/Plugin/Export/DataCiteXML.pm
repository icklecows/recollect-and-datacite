=head1 NAME

EPrints::Plugin::Export::DataCiteXML

=cut

package EPrints::Plugin::Export::DataCiteXML;

use EPrints::Plugin::Export::Feed;

@ISA = ('EPrints::Plugin::Export::Feed');

use strict;

sub new
{
        my ($class, %opts) = @_;

        my $self = $class->SUPER::new(%opts);
		
		# Expanded and updated for Schema 3.1
		
        $self->{name} = 'Data Cite XML';
        $self->{accept} = [ 'dataobj/eprint'];
        $self->{visible} = 'all';
        $self->{suffix} = '.xml';
        $self->{mimetype} = 'application/xml; charset=utf-8';
  
      return $self;
}

sub output_dataobj
{
        my ($self, $dataobj, %opts) = @_;

 		my $repo = $self->{repository};
 		my $xml = $repo->xml;

		my $thisdoi = $repo->get_conf( "datacitedoi", "prefix")."/". $repo->get_conf( "datacitedoi", "repoid")."/".$dataobj->id;
		
		# Updated links to point to kernel-3.1
		
		my $entry = $xml->create_element( "resource", xmlns=>"http://datacite.org/schema/kernel-3.1", "xmlns:xsi"=>"http://www.w3.org/2001/XMLSchema-instance", "xsi:schemaLocation"=>"http://datacite.org/schema/kernel-3.1 http://schema.datacite.org/meta/kernel-3.1/metadata.xsd" );
		
	    $entry->appendChild( $xml->create_data_element( "identifier", $dataobj->get_value( $repo->get_conf( "datacitedoi", "eprintdoifield") ) , identifierType=>"DOI" ) );
		

		my $creators = $xml->create_element( "creators" );
		
		# Added a check here so that we can add corporate creators as well as individuals.
		my $creatorscheck = 0;
		
		if( $dataobj->exists_and_set( "creators" ) )
        {
	
                my $names = $dataobj->get_value( "creators" );
                foreach my $name ( @$names )
                {
                        my $author = $xml->create_element( "creator" );

                        my $name_str = EPrints::Utils::make_name_string( $name->{name});
                        $author->appendChild( $xml->create_data_element(
                                                "creatorName",
                                                $name_str ) );
						# Added code here for ORCID and ISNI (requires additional subfields to be added to Creators in eprint_fields.pl)
						if ( EPrints::Utils::is_set( $name->{orcid} )) {
						my $orcid = $name->{orcid};
						$author->appendChild( $xml->create_data_element( "nameIdentifier", "$orcid", nameIdentifierScheme=>"ORCID", schemeURI=>"http://orcid.org/" ));
						}
						if ( EPrints::Utils::is_set( $name->{isni} )) {
						my $isni = $name->{isni};
						$author->appendChild( $xml->create_data_element( "nameIdentifier", "$isni", nameIdentifierScheme=>"ISNI", schemeURI=>"http://www.isni.org/" ));
						}
						# end ORCID and ISNI code

                        $creators->appendChild( $author );
                }
        }
		
		# Added a section for corporate creators, which needs to be added as a field "corp_creators" to the ReCollect plugin to do anything
		if ( $dataobj->exists_and_set( "corp_creators" ) {
			my $corps = $dataobj->get_value( "corp_creators" );
			foreach my $corp ( @$corps )
			{
				my $corpcreator = $xml->create_element( "creator" );
				$corpcreator->appendChild( $xml->create_data_element( "creatorName", $corp );
				$creators->appendChild( $corpcreator );
				$creatorscheck += 1;
			}
		}
		
		$entry->appendChild( $creators ) unless ( $creatorscheck == 0 );

		if ($dataobj->exists_and_set( "title" )) {
			my $titles = $xml->create_element( "titles" );
		 	$titles->appendChild(  $xml->create_data_element( "title",  $dataobj->render_value( "title" )  ) );
		
		# As there is a field for alternative title, this adds it if there is one.
		
		if ($dataobj->exists_and_set( "alt_title" )) {
			$titles->appendChild(  $xml->create_data_element( "title", $dataobj->render_value( "alt_title" ), titleType=>"AlternativeTitle"  ) );
		}
			$entry->appendChild( $titles );
		}
		
		# It isn't clear why this is a configurable field, rather than taking the information from the EPrint.
		$entry->appendChild( $xml->create_data_element( "publisher", $repo->get_conf( "datacitedoi", "publisher") ) );
	
		if ($dataobj->exists_and_set( "datestamp" )) {
		    $dataobj->get_value( "datestamp" ) =~ /^([0-9]{4})/;
			$entry->appendChild( $xml->create_data_element( "publicationYear", $1 ) ) if $1;
		
		}


		if ($dataobj->exists_and_set( "subjects" )) {
			my $subjects = $dataobj->get_value("subjects");
			# Added ability to define subject scheme - also added to configuration in z_datacitedoi.pl
			my $subjectscheme = $repo->get_conf( "datacitedoi", "subjecttree");
			my $subjectschemeuri = $repo->get_conf( "datacitedoi", "subjecttreeuri");
			# end subject scheme
			if( EPrints::Utils::is_set( $subjects ) ){
				my $subjects_tag = $xml->create_element( "subjects" );
				foreach my $val (@$subjects){
		                my $subject = EPrints::DataObj::Subject->new( $repo, $val );
				           next unless defined $subject;
				       	# Added subjectScheme and schemeURI to export
						$subjects_tag->appendChild(  $xml->create_data_element( "subject",  $subject->render_description, subjectScheme=>$subjectscheme, schemeURI=>$subjectschemeuri  ) );
				}
				$entry->appendChild( $subjects_tag );
			}
		}
	  
	
		my $thisresourceType = $repo->get_conf( "datacitedoi", "typemap", $dataobj->get_value("type") ); 
		if( $thisresourceType ne undef ){
			$entry->appendChild( $xml->create_data_element( "resourceType", $thisresourceType->{'v'},  resourceTypeGeneral=>$thisresourceType->{'a'}) );
		}
		

		# Added a range of types of contributors.
		my $contributors = $xml->create_element( "contributors" );
		
		# This is just a counter to make sure there is some kind of contributor.  If not, it won't add the element at all.
		my $contributorscheck = 0;
		
		# Firstly the funder, which is sensibly a separate type.
		if ( $dataobj->exists_and_set( "funders" )) {
			my $funders = $dataobj->get_value( "funders" );
			foreach my $fund ( @$funders )
                {
                        my $funder = $xml->create_element( "contributor", contributorType=>"Funder");
						$funder->appendChild( $xml->create_data_element( "contributorName", $fund ) );
						
                        $contributors->appendChild( $funder );
						$contributorscheck += 1;
                }
		}
		
		# Then check for personal contributors.  Not all the contribution types apply to individuals.  It's important to check you have the correct lists
		# Added files for individual contribution types to /archives/[name]/cfg/namedsets/contributor_type_datacite
		if ( $dataobj->exists_and_set( "contributors" ) ) {
			my $contribs = $dataobj->get_value( "contributors" );
			foreach my $contrib ( @$contribs )
				{
					my $conttype = $contrib->{type};
					my $contperson = $xml->create_element( "contributor", contributorType=>"$conttype" );
					my $pers_str = EPrints::Utils::make_name_string( $contrib->{name});
                    $contperson->appendChild( $xml->create_data_element( "contributorName", $pers_str ) );
					# Added code here for ORCID and ISNI (again, this needs to be added to eprint_fields.pl )
					if ( EPrints::Utils::is_set( $contrib->{orcid} )) {
					my $orcid = $contrib->{orcid};
					$contperson->appendChild( $xml->create_data_element( "nameIdentifier", "$orcid", nameIdentifierScheme=>"ORCID", schemeURI=>"http://orcid.org/" ));
					}
					if ( EPrints::Utils::is_set( $contrib->{isni} )) {
					my $isni = $contrib->{isni};
					$contperson->appendChild( $xml->create_data_element( "nameIdentifier", "$isni", nameIdentifierScheme=>"ISNI", schemeURI=>"http://www.isni.org/" ));
					}
					# end ORCID and ISNI code
                    $contributors->appendChild( $contperson );
					$contributorscheck += 1;
				}
		}
		
		# Added this field to the ReCollect plugin as many of the contribution types were for organisations rather than individuals.
		# Added files for individual contribution types to /archives/[name]/cfg/namedsets/corpcontributor_type_datacite
		if ( $dataobj->exists_and_set( "corp_contributors" ) ) {
			my $corpcontribs = $dataobj->get_value( "corp_contributors" );
			foreach my $corpcontrib ( @$corpcontribs )
				{
					my $corpconttype = $corpcontrib->{type};
					my $contorg = $xml->create_element( "contributor", contributorType=>"$corpconttype" );
					my $corpcont = $corpcontrib->{corpname};
                    $contorg->appendChild( $xml->create_data_element( "contributorName", "$corpcont" ) );

                    $contributors->appendChild( $contorg );
					$contributorscheck += 1;
				}
		}
		
		# Checks that at least one type of contributor has been added before adding the list.
		$entry->appendChild( $contributors ) unless ( $contributorscheck == 0 );
		
		# The ReCollect plugin collects languages as a text field.  If you don't change this to use the namedset, don't use this part of the export.
		if ( $dataobj->exists_and_set( "language" ) ) {
			my $lang = $dataobj->get_value( "language" );
			my $language = $xml->create_data_element( "language", $lang );
			$entry->appendChild( $language );
		}
		
		# This could be expanded to provide other dates.  I'm not sure it's yet clear how this will be useful, but this can managed collection dates.
		# Temporal coverage had no equivalent dateType, so wasn't included
		my $dates = $xml->create_element( "dates" );
		my $datescheck = 0;
		if ( $dataobj->exists_and_set( "collection_date" ) ) {
			my $coldates = $dataobj->get_value( "collection_date" );
			my $collectfrom = $coldates->{date_from};
			my $collectto = $coldates->{date_to};
			# This section just formats the date appropriately depending on whether it's just a single number or a range.
				if ( EPrints::Utils::is_set( $collectfrom ) && EPrints::Utils::is_set( $collectto ))  {
					my $collectiondates = $xml->create_data_element( "date", $collectfrom."/".$collectto, dateType=>"Collected" );
					$dates->appendChild( $collectiondates );
					$datescheck += 1;
				} elsif ( EPrints::Utils::is_set($collectfrom)) {
					my $collectiondates = $xml->create_data_element( "date", $collectfrom, dateType=>"Collected" );
					$dates->appendChild( $collectiondates );
					$datescheck += 1;
				} elsif ( EPrints::Utils::is_set($collectto)){
					my $collectiondates = $xml->create_data_element( "date", $collectto, dateType=>"Collected" );
					$dates->appendChild( $collectiondates );
					$datescheck += 1;
				}
		}

		$entry->appendChild( $dates ) unless ( $datescheck == 0 );

		my $alternateIdentifiers = $xml->create_element( "alternateIdentifiers" );
		$alternateIdentifiers->appendChild(  $xml->create_data_element( "alternateIdentifier",  $dataobj->get_url() , alternateIdentifierType=>"URL" ) );
		$entry->appendChild( $alternateIdentifiers );

		# Related identifiers have been added for all the files by type.  Warning: Local decisions ahead!
		# We added an extra type ( "code" ) to the default list, as this is a big set for us
		# We do not plan to use "Full Archive" or "README"
		my $relidlist = $xml->create_element( "relatedIdentifiers" );
		my $relidcheck = 0;
		
		if ( scalar( $dataobj->get_all_documents() ) gt 0 ) {
			foreach my $doc ( $dataobj->get_all_documents ) {
				if ( $doc->get_value( "security" ) eq "public" ) {
						if ($doc->get_value( "content" ) eq "data" ) {
						my $fileurl = $doc->get_url();
						$relidlist->appendChild( $xml->create_data_element( "relatedIdentifier", "$fileurl", relatedIdentifierType=>"URL", relationType=>"HasPart") );
						$relidcheck += 1;
						}
						if ($doc->get_value( "content" ) eq "code" ) {
						my $fileurl = $doc->get_url();
						$relidlist->appendChild( $xml->create_data_element( "relatedIdentifier", "$fileurl", relatedIdentifierType=>"URL", relationType=>"HasPart") );
						$relidcheck += 1;
						}
						if ($doc->get_value( "content" ) eq "documentation" ) {
						my $fileurl = $doc->get_url();
						$relidlist->appendChild( $xml->create_data_element( "relatedIdentifier", "$fileurl", relatedIdentifierType=>"URL", relationType=>"IsDocumentedBy") );
						$relidcheck += 1;
						}
						if ($doc->get_value( "content" ) eq "metadata" ) {
						my $fileurl = $doc->get_url();
						$relidlist->appendChild( $xml->create_data_element( "relatedIdentifier", "$fileurl", relatedIdentifierType=>"URL", relationType=>"HasMetadata") );
						$relidcheck += 1;
						}
				}
			}
		}
		
		# This part adds a relatedIdentifier if the eprint is a new version of something.
		if ( $dataobj->exists_and_set( "succeeds" ) ) {
			my $oldversion = $dataobj->get_value( "succeeds" );
			my $oldversiondoi = $repo->get_conf( "datacitedoi", "prefix")."/". $repo->get_conf( "datacitedoi", "repoid")."/".$oldversion;
			$relidlist->appendChild( $xml->create_data_element( "relatedIdentifier", "$oldversiondoi", relatedIdentifierType=>"DOI", relationType=>"IsNewVersionOf") );
			$relidcheck += 1;
		}
		
		$entry->appendChild( $relidlist ) unless ( $relidcheck == 0 );
		
		
		# This lists the size of each file in turn.
		if ( scalar( $dataobj->get_all_documents() ) gt 0 ) {
			my $sizelist = $xml->create_element( "sizes" );
			my $sizecheck = 0;
			foreach my $doc ( $dataobj->get_all_documents ) {
				if ( $doc->get_value( "security" ) eq "public" ) {
					my $filename = $doc->get_value( "main" );
					my %files = $doc->files;
					my $size_in_bytes = ($files{$doc->get_main("filesize")});
					my $size = EPrints::Utils::human_filesize($size_in_bytes);
					$sizelist->appendChild( $xml->create_data_element( "size", "$filename - $size") );
					$sizecheck += 1;
				}
			}
			$entry->appendChild( $sizelist ) unless ( $sizecheck == 0 );
		}
		
		# This lists the mime_type of each file
		if ( scalar( $dataobj->get_all_documents() ) gt 0 ) {
			my $formatlist = $xml->create_element( "formats" );
			my $formatcheck = 0;
			foreach my $doc ( $dataobj->get_all_documents ) {
				if ( $doc->get_value( "security" ) eq "public" ) {
					my $filename = $doc->get_value( "main" );
					my $format = $doc->get_value( "mime_type" );
					$formatlist->appendChild( $xml->create_data_element( "format", "$filename - $format") );
					$formatcheck += 1;
				}
			}
			$entry->appendChild( $formatlist ) unless ( $formatcheck == 0 );
		}
		
		# This is an additional field added to the ReCollect plugin.  It is a float so can have minor and major versions
		# Versioning also added to the NewVersion.pm code to enable major versions to be tracked more easily.
		if ( $dataobj->exists_and_set( "version" ) ) {
			my $versionid = $dataobj->get_value( "version" );
			my $version = $xml->create_data_element( "version", "$versionid" );
			$entry->appendChild( $version );
		}
		
		# By default license is a compulsory field, but this accounts for making it optional.
		# Had to add rather a lot of definitions to the z_datacitedoi.pl configuration file to make this work well, but this is because we plan to use a limited number of licences
		# There may be a more elegant way of doing this!
		if ( scalar( $dataobj->get_all_documents() ) gt 0 ) {
			my $rightslist = $xml->create_element( "rightsList" );
			my $rightscheck = 0;
			foreach my $doc ( $dataobj->get_all_documents ) {
				if ( $doc->get_value( "security" ) eq "public" ) {
						my $filename = $doc->get_value( "main" );
						my $licence = $doc->get_value( "license" );
						if ( EPrints::Utils::is_set( $licence ) ) {
						my $licenceuri = $repo->get_conf( "datacitedoi", "licenceuri", $licence );
						$rightslist->appendChild( $xml->create_data_element( "rights", "$filename is licensed under a $licenceuri->{'d'}", rightsURI=>$licenceuri->{'u'} ));
						$rightscheck += 1;
						} else {
						$rightslist->appendChild( $xml->create_data_element( "rights", "$filename has no associated licence.  Please contact the archive for advice." ));
						$rightscheck += 1;
						}
				}
			}
			$entry->appendChild( $rightslist ) unless ( $rightscheck == 0 );
		}
		
		# There may well be other fields that lend themselves to be added here, but these were two obvious ones
		my $descriptions = $xml->create_element( "descriptions" );
		my $descriptionscheck = 0;
		
		if ( $dataobj->exists_and_set( "abstract" ) ) {
			my $abstract = $dataobj->get_value( "abstract" );
			my $descripabs = $xml->create_data_element( "description", "$abstract", descriptionType=>"Abstract" );
			$descriptions->appendChild( $descripabs );
			$descriptionscheck += 1;
		}
		
		if ( $dataobj->exists_and_set( "collection_method" ) ) {
			my $collectionmethod = $dataobj->get_value( "collection_method" );
			my $descripcolmeth = $xml->create_data_element( "description", "$collectionmethod", descriptionType=>"Methods" );
			$descriptions->appendChild( $descripcolmeth );
			$descriptionscheck += 1;
		}
		
		$entry->appendChild( $descriptions ) unless ( $descriptionscheck == 0 );
		
		# Each of the Point and Box geolocations could have a description as well, but have not implemented this as it's unclear how useful this would be.
		# Point fields added as geoLocation field in the ReCollect plugin
		my $geolocations = $xml->create_element( "geoLocations" );
		my $geolocationscheck = 0;
		if ( $dataobj->exists_and_set( "geolocation" ) ) {
			my $geolocs = $dataobj->get_value( "geolocation" );
			foreach my $loc ( @$geolocs )
			{
				my $geolocation = $xml->create_element( "geoLocation" );
				my $geolat = $loc->{latitude};
				my $geolong = $loc->{longitude};
				my $geopoint = $xml->create_data_element( "geoLocationPoint", $geolat." ".$geolong );
				$geolocation->appendChild( $geopoint );
				$geolocations->appendChild( $geolocation );
				$geolocationscheck += 1;
			}
		}

		if ( $dataobj->exists_and_set( "bounding_box" ) ) {
			my $geoboxes = $dataobj->get_value( "bounding_box" );
			foreach my $box ( @$geoboxes )
			{
				my $geolocation = $xml->create_element( "geoLocation" );
				my $geonorth = $box->{north_edge};
				my $geosouth = $box->{south_edge};
				my $geoeast = $box->{east_edge};
				my $geowest = $box->{west_edge};
				my $geobox = $xml->create_data_element( "geoLocationBox", $geosouth." ".$geowest." ".$geonorth." ".$geoeast );
				$geolocation->appendChild( $geobox );
				$geolocations->appendChild( $geolocation );
				$geolocationscheck += 1;
			}
		}
		
		if ( $dataobj->exists_and_set( "geographic_cover" ) ) {
			my $geolocation = $xml->create_element( "geoLocation" );
			my $geocover = $dataobj->get_value( "geographic_cover" );
			my $geoplace = $xml->create_data_element( "geoLocationPlace", "$geocover" );
			$geolocation->appendChild( $geoplace );
			$geolocations->appendChild( $geolocation );
			$geolocationscheck += 1;
		}
		$entry->appendChild( $geolocations ) unless ( $geolocationscheck == 0 );

       return '<?xml version="1.0" encoding="UTF-8"?>'."\n".$xml->to_string($entry);
}

1;
