=head1 NAME

EPrints::Plugin::Event::DataCiteEvent

=cut

package EPrints::Plugin::Event::DataCiteEvent;
 
use EPrints::Plugin::Event;
use LWP;
use Crypt::SSLeay;

@ISA = qw( EPrints::Plugin::Event );

  # Added override HTTPS setting in LWP to work with a proxy - uncomment and set local proxy server name to activate.
#BEGIN {
#  $ENV{PERL_NET_HTTPS_SSL_SOCKET_CLASS} = "Net::SSL";
#  $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0;
#  $ENV{HTTPS_PROXY} = 'name.of.proxy.server:3128';
#}

sub datacite_doi
 {
       my( $self, $eprint) = @_;

		my $repository = $self->repository();
		
		my $thisdoi = $repository->get_conf( "datacitedoi", "prefix")."/". $repository->get_conf( "datacitedoi", "repoid")."/".$eprint->id;
	
		my $eprintdoifield = $repository->get_conf( "datacitedoi", "eprintdoifield");
		
		my $task;
		
		my $shoulddoi = $repository->get_conf( "datacitedoi", "eprintstatus",  $eprint->value( "eprint_status" ));
		
		#Check Doi Status
		if(!$shoulddoi){ return; }
		
		#check if doi has been set;
		if( $eprint->exists_and_set( $eprintdoifield )) {
			if( $eprint->value( $eprintdoifield ) ne $thisdoi ){
				#Skipping because its has a diff doi;
				return;
			}
		}else{
			$eprint->set_value($eprintdoifield, $thisdoi);
			$eprint->commit();
		}
		
		
		my $xml = $eprint->export( "DataCiteXML" );
		my $url = $repository->get_conf( "datacitedoi", "apiurl");
		my $user_name = $repository->get_conf( "datacitedoi", "user");
		my $user_pw = $repository->get_conf( "datacitedoi", "pass");
		
		#register metadata;
		$response_code =  datacite_request("POST", $url."metadata", $user_name, $user_pw, $xml, "application/xml;charset=UTF-8");
		
		#register doi
		my $doi_reg = "doi=$thisdoi\nurl=".$eprint->uri();
		$response_code =  datacite_request("POST", $url."doi", $user_name, $user_pw, $doi_reg, "text/plain;charset=UTF-8");

		# Added a little logging section originally to help with debugging, but kept and reworded to provide a record of DOIs minted.
		my $mintingtime = scalar localtime(time);

		open(DOILOG, ">>/usr/share/eprints/archives/[name]/var/DOIminting.log") || die "Cannot open file: $!";
		print DOILOG "Registration of doi:\n$doi_reg\nTimestamp: $mintingtime\nResponse code: $response_code\n\n";
		close(DOILOG);

		return undef;
}


sub datacite_request {
  my ($method, $url, $user_name, $user_pw, $content, $content_type) = @_;

  # build request
  my $headers = HTTP::Headers->new(
    'Accept'  => 'application/xml',
    'Content-Type' => $content_type
  );

  my $req = HTTP::Request->new(
    $method => $url,
    $headers, $content
  );
  $req->authorization_basic($user_name, $user_pw);

  # pass request to the user agent and get a response back
  my $ua = LWP::UserAgent->new;
  my $res = $ua->request($req);

  return $res->content();
}




1;
