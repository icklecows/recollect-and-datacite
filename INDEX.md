#### archives/[name]/cfg/cfg.d ####

* eprint_fields.pl
* eprint_fields_automatic.pl
* x_recollect.pl
* z_datacite_core.pl
* z_datacitedoi.pl

#### archives/[name]/cfg/citations ####
* recollect_default.xml
* recollect_summary_page.xml

#### archives/[name]/cfg/namedsets ####
* contributor_type_datacite
* corpcontributor_type_datacite
* languages
* recollect_content

#### archives/[name]/cfg/workflows/eprint ####
* default.xml

#### archives/[name]/var ####
* DOIminting.log
	
#### lib/plugins/EPrints/Plugin/Event ####
* DataCiteEvent.pm

#### lib/plugins/EPrints/Plugin/Export ####
* DataCiteXML.pm
	
#### perl_lib/EPrints/Plugin/Screen/EPrint/ ####
* NewVersion.pm

* README.md
* INDEX.md
