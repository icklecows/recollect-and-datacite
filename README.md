# README #

This is not an application, just an attempt to organise all the bits of EPrints that I've changed to make the Recollect and DataCite Plugins work more effectively for our plans.

### What is this repository for? ###

* This is a development repository to document changes made which may also be useful to others.
* This is for the EPrints software (http://www.eprints.org/)
* It is based on the ReCollect plugin (http://wiki.eprints.org/w/ReCollect) and DataCite plugin (https://github.com/eprints/datacite)


## Changes made to ReCollect ##

#### /archives/[name]/cfg/cfg.d/x_recollect.pl ####

* Added field for single geographic point (geolocation)

* Replaced language field with one that uses the named set.

* Added field for corporate contributors (mirroring corporate creators and contributors in eprint_fields.pl) using namedset containing DataCite controlled values

* Added field for version, using float to allow for major and minor versions.

* Added new fields to summary_page_metadata_full

* Added Export bar to enable per EPrint export of records

* Added DOI field as a link (as I wanted to make use of the DOI field rather than id_number)

* Added document type metadata (missing) and document type code (local) to list of document types where needed


## Changes made to DataCite ##

#### /lib/plugins/EPrints/Plugin/Event/DataCiteEvent.pm ####

* Added section to create a log.

* Added section to allow requests to be sent through a web proxy if needed.

#### /lib/plugins/EPrints/Plugin/Export/DataCiteXML.pm ####

* Added and expanded most elements to provide a fuller export in line with Schema 3.0.  Comments in line explain the changes in detail.

#### /archives/[name]/cfg/cfg.d/z_datacite_core.pl ####

* Updated some deprecated code, probably makes no difference.

#### /archives/[name]/cfg/cfg.d/z_datacitedoi.pl ####

* Added option to use doi field instead of id_number

* Added data_collection to list of resource types

* Added configurations for subject tree definition and licence URIs and definitions.


## Changes made to EPrints core ##

#### /perl_lib/EPrints/Plugin/Screen/EPrint/NewVersion.pm ####

* Added lines to allow version numbers to be generated for new versions (major only).

* Added lines to remove DOIs from new versions if they are minted by the repository.


## Changes made to Archive configuration ##

#### /archives/[name]/cfg/cfg.d/eprint_fields.pl ####

* Added ORCID and ISNI to Creators and Contributors.

* Changed set name to use DataCite controlled list for contributors

#### /archives/[name]/cfg/cfg.d/eprint_fields_automatic.pl ####

* Added automatic defaults for language (English) and version (1) where not set.

#### /archives/[name]/cfg/namedsets/contributor_type_datacite ####

* New file which lists the contributor types for individuals as defined in Schema 3.0

#### /archives/[name]/cfg/namedsets/corpcontributor_type_datacite ####

* New file which lists the contributor types for organisations as defined in Schema 3.0

#### /archives/[name]/cfg/namedsets/languages ####

* Entirely local change to put the languages in English alphabetical order.

#### /archives/[name]/cfg/namedsets/recollect_content ####

* Added code as a file type.

#### /archives/[name]/var/DOIminting.log ####

* Arbitrary file for logging from DataCiteEvent.pm